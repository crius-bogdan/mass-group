"use strict";
// import Pageable from '../js/pageable.min.js';
window.addEventListener("DOMContentLoaded", function () {
    var doc = document,
        mainSliderSelector = '.main-slider',
        navSliderSelector = '.nav-slider',
        interleaveOffset = 0.5,
        anchorPrev = doc.querySelector('.anchors__item--prev'),
        anchorNext = doc.querySelector('.anchors__item--next'),
        anchors = doc.querySelectorAll('.anchors__link'),
        wrapBtns = doc.getElementsByClassName('swiper-wrapper')[0],
        wrapModal = doc.getElementsByClassName('modals')[0],
        contentModal = doc.querySelectorAll('.content-mod'),
        modalCall = doc.querySelector('.modal-call'),
        modalCallClose = doc.querySelector('.close-call'),
        modalCallForm = doc.querySelector('.modal-call__form'),
        callBtn = doc.querySelector('.call__btn'),
        callBtn2 = doc.querySelector('.offer__btn'),
        scrollPlease = doc.querySelector('.mouse-wrap'),
        mainWrap = doc.querySelector('.main-wrap'),
        overlay = doc.getElementsByClassName('overlay')[0],
        burgerMenu = doc.querySelector('.square'),
        closeMenu = doc.querySelector('.menu-close'),
        menuLink = doc.querySelector('.menu'),
        menuWrap = doc.querySelector('.menu__wrap'),
        body = doc.body,
        valueElement, map;
        function getMobileOperatingSystem() {
            var userAgent = navigator.userAgent || navigator.vendor || window.opera;

            // Windows Phone must come first because its UA also contains "Android"
            if (/windows phone/i.test(userAgent)) {
                return "Windows Phone";
            }

            if (/android/i.test(userAgent)) {
                return "Android";
            }

            // iOS detection from: http://stackoverflow.com/a/9039885/177710
            if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
                return "iOS";
            }

            return "unknown";
        }
        if (getMobileOperatingSystem() == "iOS" || getMobileOperatingSystem() == "unknown") {
            // var iosScript = '<script src=\"js/pageable.min.js\"><\/script>';
            // body.innerHTML = iosScript;
            var pages = new Pageable(".main-wrap", {
                animation: (window.innerWidth < 450) ? 750 : 1500,
                delay: (window.innerWidth < 450) ? 200 : 500,
                pips: true,
                events: {
                    wheel: true,
                    mouse: false,
                    touch: true,
                    keydown: false,
                },
                navPrevEl: 'button[data-scroll="prev"]',
                navNextEl: 'button[data-scroll="next"]',
                onInit: function () {
                    var itIs = this;
                    this.pages[0].classList.remove('pg-active');
                    setTimeout(function () {
                        itIs.pages[0].classList.add('pg-active');
                    }, 2000);
                },
                onStart: function (data) {
                    this.pages.forEach(function (page) {
                        page.classList.remove("pg-active");
                    });
                },
                onScroll: function (data) {
                    anchors.forEach(function (anchor, i) {
                        anchor.classList.toggle("active", i === data.index);
                    });
                    setTimeout(slidersToIndex, 500);
                },
                onFinish: function (data) {
                    this.pages.forEach(function (page) {
                        page.classList.add("pg-active");
                    });
                }
            });

        } else if (getMobileOperatingSystem() == "Android") {
            // var androidScript = '<script src=\"js/onepagescroll.js\"> <\/script>';
            // body.innerHTML = androidScript;
            var pages = onePageScroll(".main-wrap", {
                sectionContainer: "section", // sectionContainer accepts any kind of selector in case you don't want to use section
                easing: "cubic-bezier(0.785, 0.135, 0.150, 0.860)", // Easing options accepts the CSS3 easing animation such "ease", "linear", "ease-in", 
                // "ease-out", "ease-in-out", or even cubic bezier value such as "cubic-bezier(0.175, 0.885, 0.420, 1.310)"
                animationTime: 1100, // AnimationTime let you define how long each section takes to animate
                pagination: true, // You can either show or hide the pagination. Toggle true for show, false for hide.
                updateURL: false, // Toggle this true if you want the URL to be updated automatically when the user scroll to each page.
                beforeMove: function (index) {
                    anchors.forEach(function (anchor) {
                        var data = anchor.getAttribute('data-index');
                        anchor.classList.toggle("active", data == index);
                    });

                    // if (index != 1) {
                    //     anchorPrev.setAttribute('data-index', --index);
                    //     anchorNext.setAttribute('data-index', ++index);
                    // } else {
                    //     anchorPrev.setAttribute('data-index', index);
                    //     anchorNext.setAttribute('data-index', 2);
                    // }
                }, // This option accepts a callback function. The function will be called before the page moves.
                afterMove: function (index) {
                    slidersToIndex();

                }, // This option accepts a callback function. The function will be called after the page moves.
                loop: false, // You can have the page loop back to the top/bottom when the user navigates at up/down on the first/last page.
                keyboard: true, // You can activate the keyboard controls
                responsiveFallback: false // You can fallback to normal page scroll by defining the width of the browser in which
                // you want the responsive fallback to be triggered. For example, set this to 600 and whenever 
                // the browser's width is less than 600, the fallback will kick in.
            });
        }
        
    
    // pages.on('touch', function(event){
    //     console.log(event);
    // });
    var swipers = new Swiper('.swiper-cont', {
        initialSlide: 0,
        // spaceBetween: 30,
        loop: true,
        slidesPerView: 'auto',
        resistance: false,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        on: {
            touchStart: function (event) {
                // swipers.slideTo(2, 300);
                var target = event.target;
                while(target != doc) {
                    // console.log(target)
                    if (target.classList.contains('slide__btn')) {
                        valueElement = target.getAttribute('data-num');
                        wrapModal.classList.add('modals-active__' + valueElement);
                        return;
                    }
                    target = target.parentNode;
                }

            },
        }
    });
    var s2 = new Swiper('.propose', {
        init: false,
        initialSlide: 0,
        loop: true,
        // spaceBetween: 15,
        slidesPerView: 'auto',
        resistance: false,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });
    var s3 = new Swiper('.advantage', {
        init: false,
        initialSlide: 0,
        spaceBetween: 0,
        centeredSlides: true,
        slidesPerView: 1,
        resistance: false,
        pagination: {
            el: '.swiper-pagination',
        },
    });
    function slidersToIndex() {
        for (var i = 0; i < swipers.length; i++) {
            swipers[i].slideTo(0, 300);
        }
        if (window.innerWidth <= 1024) {
            // console.log(s2)
            s2.slideTo(0, 300);
        }
        if (window.innerWidth <= 426) {
            s3.slideTo(0, 300);
        }
        
    }
    // Main Slider
    var mainSliderOptions = {
        loop: true,
        speed: 1000,
        autoplay: {
            delay: 3000
        },
        loopAdditionalSlides: 10,
        grabCursor: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.slider-btn-prev',
            prevEl: '.slider-btn-next',
        },
        on: {
            init: function () {
                this.autoplay.stop();
            },
            imagesReady: function () {
                this.el.classList.remove('loading');
                this.autoplay.start();
            },
            slideChangeTransitionEnd: function () {
                var swiper = this,
                    captions = swiper.el.querySelectorAll('.caption');
                for (var i = 0; i < captions.length; ++i) {
                    captions[i].classList.remove('show');
                }
                swiper.slides[swiper.activeIndex].querySelector('.caption').classList.add('show');
            },
            progress: function () {
                var swiper = this;
                for (var i = 0; i < swiper.slides.length; i++) {
                    var slideProgress = swiper.slides[i].progress,
                        innerOffset = swiper.width * interleaveOffset,
                        innerTranslate = slideProgress * innerOffset;
                    swiper.slides[i].querySelector(".slide-bgimg").style.transform =
                        "translate3d(" + innerTranslate + "px, 0, 0)";
                }
            },
            touchStart: function () {
                var swiper = this;
                for (var i = 0; i < swiper.slides.length; i++) {
                    swiper.slides[i].style.transition = "";
                }
            },
            setTransition: function (speed) {
                var swiper = this;
                for (var i = 0; i < swiper.slides.length; i++) {
                    swiper.slides[i].style.transition = speed + "ms";
                    swiper.slides[i].querySelector(".slide-bgimg").style.transition =
                        speed + "ms";
                }
            }
        }
    };
    var mainSlider = new Swiper(mainSliderSelector, mainSliderOptions);

    // Navigation Slider
    var navSliderOptions = {
        loop: true,
        loopAdditionalSlides: 10,
        speed: 1000,
        spaceBetween: 5,
        slidesPerView: 5,
        centeredSlides: true,
        touchRatio: 0.2,
        slideToClickedSlide: true,
        direction: 'vertical',
        on: {
            imagesReady: function () {
                this.el.classList.remove('loading');
            },
            click: function () {
                mainSlider.autoplay.stop();
            }
        }
    };
    var navSlider = new Swiper(navSliderSelector, navSliderOptions);

    // Matching sliders
    mainSlider.controller.control = navSlider;
    navSlider.controller.control = mainSlider;
    if (window.innerWidth <= 1024) {
        s2.init();
    }
    if (window.innerWidth <= 426) {
        s3.init();
    }
    scrollPlease.addEventListener(window.innerWidth <= 1024 ? 'touchend' : 'click', function () {
        pages.next();
    });
    //open modal
    burgerMenu.addEventListener('click', function() {
        menuWrap.classList.add('menu__wrap__active');
    });
    Array.from(doc.querySelectorAll('.offer__btn')).forEach(function (el) {
        el.addEventListener(window.innerWidth <= 1024 ? 'touchend' : 'click', function () {
            modalCall.classList.remove('modal-call_success');
            modalCall.classList.add('modal-call_active');
            overlay.classList.add('show');
        });
    });
    doc.addEventListener('click', function (event) {
        var target = event.target;
        while (target != this) {
            if (target.classList.contains('menu__link') || target.classList.contains('menu-close') || target.classList.contains('logo__link')) {
                menuWrap.classList.remove('menu__wrap__active');
                if (valueElement) wrapModal.classList.remove('modals-active__' + valueElement);
                if (getMobileOperatingSystem() == "Android") {
                    event.preventDefault();
                    var index = target.getAttribute('data-index');
                    if (target.classList.contains('logo__link')) moveTo('.main-wrap', 1);
                    moveTo(".main-wrap", index);
                    return;
                }
                return;
            }
            if (target.classList.contains('anchors__item')) {
                if(target.classList.contains('anchors__item--prev')) {
                    pages.prev();
                    return;
                }
                if(target.classList.contains('anchors__item--next')) {
                    pages.next();
                    return;
                }
            }
            if (target.classList.contains('call__btn')) {
                if (menuWrap.classList.contains('menu__wrap__active')) menuWrap.classList.remove('menu__wrap__active');
                modalCall.classList.remove('modal-call_success');
                modalCall.classList.add('modal-call_active');
                overlay.classList.add('show');
                return;
            }
            if (target.classList.contains('content-mod__top-back')) {
                // alert('клик ' + target);
                wrapModal.classList.remove('modals-active__' + valueElement);
                return;
            }
            target = target.parentNode;
        }
    });
    (function () {
        function validEmail(email) {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return re.test(email);
        }

        function validPhone(phone) {
            var re = /^([+]?[0-9\s-\(\)]{3,25})*$/;
            return re.test(phone);
        }
        

        function validateHuman(honeypot) {
            if (honeypot) { //if hidden form filled up
                console.log("Robot Detected!");
                return true;
            } else {
                console.log("Welcome Human!");
            }
        }

        // get all data in form and return object
        function getFormData(form) {
            var elements = form.elements;

            var fields = Object.keys(elements).filter(function (k) {
                return (elements[k].name !== "honeypot");
            }).map(function (k) {
                if (elements[k].name !== undefined) {
                    return elements[k].name;
                    // special case for Edge's html collection
                } else if (elements[k].length > 0) {
                    return elements[k].item(0).name;
                }
            }).filter(function (item, pos, self) {
                return self.indexOf(item) == pos && item;
            });

            var formData = {};
            fields.forEach(function (name) {
                var element = elements[name];

                // singular form elements just have one value
                formData[name] = element.value;

                // when our element has multiple items, get their values
                if (element.length) {
                    var data = [];
                    for (var i = 0; i < element.length; i++) {
                        var item = element.item(i);
                        if (item.checked || item.selected) {
                            data.push(item.value);
                        }
                    }
                    formData[name] = data.join(', ');
                }
            });

            // add form-specific values into the data
            formData.formDataNameOrder = JSON.stringify(fields);
            formData.formGoogleSheetName = form.dataset.sheet || "responses"; // default sheet name
            formData.formGoogleSendEmail = form.dataset.email || ""; // no email by default

            // console.log(formData);
            return formData;
        }

        function handleFormSubmit(event) { // handles form submit without any jquery
            event.preventDefault(); // we are submitting via xhr below
            var form = event.target;
            var data = getFormData(form); // get the values submitted in the form

            /* OPTION: Remove this comment to enable SPAM prevention, see README.md
            if (validateHuman(data.honeypot)) {  //if form is filled, form will not be submitted
            return false;
            }
            */
            // console.log(data.name.replace(/\s+/g, '').length ==0 + ' имя', data.name, data.name.replace(/\s+/g, '').length === 0)
            console.log(!validPhone(data.phone))
            // console.log(!validName(data.name) && !validPhone(data.phone))
            if (!validPhone(data.phone) || data.name.replace(/\s+/g, '').length === 0) {
                var invalidPhone = form.querySelector(".error-phone"),
                    invalidName = form.querySelector(".error-name");

                if (invalidPhone && !validPhone(data.phone)) {
                    invalidPhone.style.opacity = "1";
                    if (invalidName && data.name.replace(/\s+/g, '').length === 0) {
                        invalidName.style.opacity = "1";
                        return false;
                    }
                    return false;
                }
                if (data.name.replace(/\s+/g, '').length === 0) {
                    invalidName.style.opacity = "1";
                    if (invalidPhone && !validPhone(data.phone) || data.phone.replace(/\s+/g, '').length === 0) {
                        invalidPhone.style.opacity = "1";
                        return false;
                    }
                    return false;
                }
            } else {
                // disableAllButtons(form);
                var url = form.action;
                var xhr = new XMLHttpRequest();
                xhr.open('POST', url);
                // xhr.withCredentials = true;
                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                xhr.onreadystatechange = function () {
                    // console.log(xhr.status, xhr.statusText);
                    // console.log(xhr.responseText);
                    modalCall.classList.add('modal-call_success');
                    setTimeout(function() {
                        modalCall.classList.remove('modal-call_active');
                        overlay.classList.remove('show');
                    }, 2000);
                    form.reset();
                    return;
                };
                // url encode form data for sending as post data
                var encoded = Object.keys(data).map(function (k) {
                    return encodeURIComponent(k) + "=" + encodeURIComponent(data[k]);
                }).join('&');
                xhr.send(encoded);
            }
        }
        modalCallForm.addEventListener("submit", handleFormSubmit, false);
    })();
    doc.addEventListener('click', function(event) {
        var target = event.target;
    });
    modalCallClose.addEventListener('click', function() {
        modalCall.classList.remove('modal-call_active');
        overlay.classList.remove('show');
    });
    overlay.addEventListener('click', function() {
        modalCall.classList.remove('modal-call_active');
        overlay.classList.remove('show');
    });
    function setCursorPosition(pos, elem) {
        elem.focus();
        if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
        else if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd("character", pos);
            range.moveStart("character", pos);
            range.select()
        }
    }

    function mask(event) {
        var invalidPhone = doc.querySelector(".error-phone");
        invalidPhone.style.opacity = "0";
        var matrix = this.defaultValue,
            i = 0,
            def = matrix.replace(/\D/g, ""),
            val = this.value.replace(/\D/g, "");
        def.length >= val.length && (val = def);
        matrix = matrix.replace(/[_\d]/g, function (a) {
            return val.charAt(i++) || "_"
        });
        this.value = matrix;
        i = matrix.lastIndexOf(val.substr(-1));
        i < matrix.length && matrix != this.defaultValue ? i++ : i = matrix.indexOf("_");
        setCursorPosition(i, this)
    }

    var input1 = doc.querySelector("#tel"),
        input2 = doc.querySelector("#name");

    input1.addEventListener('click', function() {
        input1.setAttribute('value', '+7(___)___-____');
        setCursorPosition(3, input1);
    });
    input2.addEventListener('click', function() {
        input2.setAttribute('value', ' ');
        var invalidName = doc.querySelector(".error-name");
        invalidName.style.opacity = "0";
    });
    function validName() {
        var shab = /[^A-Za-zА-Яа-пр-яЁё\s]/; //шаблон для сравнения 
        var value = this.value; // значение из input
        if (shab.test(value)) {
            value = value.replace(shab, ''); //заменяем на пустоту, то что не подходит
            this.value = value; // возвращаем очищенное значение в input
        }
    }
    input2.addEventListener('keyup', validName);
    input1.addEventListener("input", mask, false);
});